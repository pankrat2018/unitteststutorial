﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestTutor;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        Chisla c = new Chisla();
        [TestMethod]
        public void TestMethod1()
        {
            
            Assert.AreEqual(c.Sum(1, 1), 2); 
        }
        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual(c.Sum(1, 2), 3);
        }
    }
}
